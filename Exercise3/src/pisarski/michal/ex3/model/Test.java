package pisarski.michal.ex3.model;


public class Test implements Testable {
	
	public String publicField = "Weee, a public field is accessible everywhere!!!";
	private String privateField = "Booo, a private field should not be accessible outside this class... But it is due to reflection:)";

	@Override
	public void test() {

		System.out.println(this.getClass().getName() + " is writing something into console...");
		
	}

}
