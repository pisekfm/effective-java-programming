package pisarski.michal.ex3.main;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import pisarski.michal.ex3.model.Test;

public class Main {

	/**
	 * @param args
	 * @throws NoSuchMethodException 
	 * @throws SecurityException 
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 */
	public static void main(String[] args) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {

		Object obj = new Test();
		
		Method testMethod = obj.getClass().getMethod("test");
		testMethod.invoke(obj);
		
		
		
		
		String fieldName;
		
		fieldName = "publicField";
		System.out.println("And so the " + fieldName + " of " + obj.getClass() + " is equal to: [" + getFieldFromObject(obj, fieldName) + "]");
		
		fieldName = "privateField";
		System.out.println("And so the " + fieldName + " of " + obj.getClass() + " is equal to: [" + getFieldFromObject(obj, fieldName) + "]");
		
	}

	public static Object getFieldFromObject(Object object, String fieldName)
			throws IllegalAccessException {
		try {
			Field field = object.getClass().getDeclaredField(fieldName);
			if (field != null) {
				field.setAccessible(true);
				return field.get(object);
			}
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		}
		return null;
	}

}
