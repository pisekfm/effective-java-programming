package benchmark;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

/**
 * Klasa odpowiadajaca za drukowanie rezultatow testow do pliku badz w skroconej formie
 * @author mpisarsk
 *
 */
public class BenchmarkPrinter {

	private BenchmarkResults benchmarkResults;
	
	private PrintStream ps = null;

	@SuppressWarnings("unused")
	private BenchmarkPrinter() {}
	
	public BenchmarkPrinter(File statsFile) {
		try {
			this.ps = new PrintStream(statsFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public BenchmarkPrinter(BenchmarkResults benchmarkResults, File statsFile) {
		this(statsFile);
		this.benchmarkResults = benchmarkResults;
	}
	
	public void getFullToFile() throws IOException {
		if (ps != null) {
			ps.println(getFull());
		}
	}
	
	public void getStatsToFile() throws IOException {
		if (ps != null) {
			ps.println(getStats());
		}
	}
	
	public void getStatsOneLineToFile() throws IOException {
		
		if (ps != null) {
			ps.println(getStatsOneLine());
		}
	}
	
	public String getFull() {
		return benchmarkResults.toString();
	}
	
	public String getStats() {
		StringBuilder sb = new StringBuilder();
		sb.append(benchmarkResults.getTitle()).append(" (x").append(benchmarkResults.getTimeResults().size()).append("):").append('\n');
		sb.append("average: ").append(benchmarkResults.getAverage()).append('\n');
		sb.append("min: ").append(benchmarkResults.getMin()).append('\n');
		sb.append("max: ").append(benchmarkResults.getMax()).append('\n');
		sb.append("max/min ratio: ").append(benchmarkResults.getMaxMinRatio()).append('\n');
		return sb.toString();
	}
	
	public String getStatsOneLine() {
		StringBuilder sb = new StringBuilder();
		sb.append(benchmarkResults.getTitle()).append(" (x").append(benchmarkResults.getTimeResults().size()).append("): ");
		sb.append("average: ").append(benchmarkResults.getAverage()).append("; ");
		sb.append("min: ").append(benchmarkResults.getMin()).append("; ");
		sb.append("max: ").append(benchmarkResults.getMax()).append("; ");
		sb.append("max/min ratio: ").append(benchmarkResults.getMaxMinRatio()).append("; ");
		return sb.toString();
	}
	
	public void setBenchmarkResults(BenchmarkResults benchmarkResults) {
		this.benchmarkResults = benchmarkResults;
	}
	
	public void close() {
		ps.close();
	}
	
}
