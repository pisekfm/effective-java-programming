package ejp;
import java.io.Serializable;
import java.util.Date;


public class Person  implements Serializable  {
	
	private String name;
	private String lastname;
	private Date birthday;
	private Integer age;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	@Override
	public String toString() {
		return "Person [name=" + name + ", lastname=" + lastname
				+ ", birthday=" + birthday + ", age=" + age + "]";
	}
}
