package ejp;
import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.Date;


public class PersonExt extends Person implements Externalizable {

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeUTF(getName());
		out.writeUTF(getLastname());
		out.writeLong(getBirthday().getTime());
		out.writeShort(getAge());
	}
	
	@Override
	public void readExternal(ObjectInput in) throws IOException,
			ClassNotFoundException {
		setName(in.readUTF());
		setLastname(in.readUTF());
		setBirthday(new Date(in.readLong()));
		setAge((int) in.readShort());
	}



}
