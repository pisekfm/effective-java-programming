package ejp;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Date;


public class Main {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws ClassNotFoundException 
	 */
	public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {

		PersonSer p2 = new PersonSer();
		p2.setName("Michal");
		p2.setLastname("Pisarski");
		p2.setBirthday(new Date());
		p2.setAge(24);
		
		testSavingObjToFile(p2, "personSer.bin");
		
		PersonExt p = new PersonExt();
		p.setName("Michal");
		p.setLastname("Pisarski");
		p.setBirthday(new Date());
		p.setAge(24);
		
		testSavingObjToFile(p, "personExt.bin");
		
		File f1 = new File("personSer.bin");
		File f2 = new File("personExt.bin");
		
		System.out.println("personSer.bin size: "+f1.length());
		System.out.println("personExt.bin size: "+f2.length());
		System.out.println("personSer.bin - personExt.bin = " + (f1.length() - f2.length()));
	
	}

	private static void testSavingObjToFile(Object p, String file) throws IOException,
			FileNotFoundException, ClassNotFoundException {
		
		ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));

		out.writeObject(p);

		ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));

		Object inputObj = in.readObject();

		System.out.println(inputObj);
		
	}

}
