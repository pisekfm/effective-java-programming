package ejp;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.PriorityQueue;
import benchmark.BenchmarkPrinter;


public class Main {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		
		File statsFile = new File("statsFile.log");
		
		final ArrayList<Integer> al = new ArrayList<Integer>();
		final LinkedList<Integer> ll = new LinkedList<Integer>();
		final PriorityQueue<Integer> pq = new PriorityQueue<Integer>();

		BenchmarkPrinter bp = new BenchmarkPrinter(statsFile);
		
		int iterations = 5;
		
		for (int i = 10; i <= 100000; i*=10, iterations*=2) {

			//fill list
			CollectionUtils.fill(al, i);
			//adding
			CollectionUtils.addBegining(al, bp, i, iterations);
			CollectionUtils.addEnd(al, bp, i, iterations);
			CollectionUtils.addMiddle(al, bp, i, iterations);				
			//browsing
			CollectionUtils.browseIndex(al, bp, i, iterations);
			CollectionUtils.browseIterator(al, bp, i, iterations);
			//removing
			CollectionUtils.removeMiddle(al, bp, i, iterations);			
			CollectionUtils.removeBegining(al, bp, i, iterations);
			CollectionUtils.removeEnd(al, bp, i, iterations);
			//clear
			al.clear();
			
			//fill list
			CollectionUtils.fill(ll, i);
			//adding
			CollectionUtils.addBegining(ll, bp, i, iterations);
			CollectionUtils.addEnd(ll, bp, i, iterations);
			CollectionUtils.addMiddle(ll, bp, i, iterations);				
			//browsing
			CollectionUtils.browseIndex(ll, bp, i, iterations);
			CollectionUtils.browseIterator(ll, bp, i, iterations);
			//removing
			CollectionUtils.removeMiddle(ll, bp, i, iterations);			
			CollectionUtils.removeBegining(ll, bp, i, iterations);
			CollectionUtils.removeEnd(ll, bp, i, iterations);
			//clear
			ll.clear();
			
			//fill queue
			CollectionUtils.fill(pq, i);
			//adding
			CollectionUtils.addBegining(pq, bp, i, iterations);
			//browsing
			CollectionUtils.browseIterator(pq, bp, i, iterations);
			//removing
			CollectionUtils.removeBegining(pq, bp, i, iterations);
			//clear
			pq.clear();
			
		}
		
		bp.close();
		
	}
	
}
