package ejp;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;

import benchmark.BenchmarkCodeTest;
import benchmark.BenchmarkPrinter;
import benchmark.BenchmarkResults;
import benchmark.StandardBenchmark;

/**
 * Klasa wspomagajaca testowanie funkcji dla List i Kolejek 
 * @author mpisarsk
 *
 */
public class CollectionUtils {

	
	public static void removeBegining(final PriorityQueue<Integer> al,
			BenchmarkPrinter bp, int size, int i) throws IOException {
		testMe(bp, i, al.getClass().getSimpleName() + "["+size+"] remove begining", new BenchmarkCodeTest() {public void testCode() {al.remove(0);}});
	}
	
	public static void browseIterator(final PriorityQueue<Integer> al,
			BenchmarkPrinter bp, int size, int i) throws IOException {
		testMe(bp, i, al.getClass().getSimpleName() + "["+size+"] browse all using iterator", new BenchmarkCodeTest() {public void testCode() {
			for (Iterator<Integer> iterator = al.iterator(); iterator.hasNext();) {
				if (iterator.next() == 7) {
					break;
				}
			}
		}});
	}

	public static void addBegining(final PriorityQueue<Integer> al,
			BenchmarkPrinter bp, int size, int i) throws IOException {
		testMe(bp, i, al.getClass().getSimpleName() + "["+size+"] add begining", new BenchmarkCodeTest() {public void testCode() {al.add(5);}});
	}

	public static PriorityQueue<Integer> fill(PriorityQueue<Integer> pq, int size) {
		for (int i = 0; i < size; i++) {
			pq.add(0);
		}
		return pq;		
	}

	public static void removeEnd(final List<Integer> al,
			BenchmarkPrinter bp, int size, int i) throws IOException {
		testMe(bp, i, al.getClass().getSimpleName() + "["+size+"] remove end", new BenchmarkCodeTest() {public void testCode() {al.remove(al.size()-1);}});
	}

	public static void removeBegining(final List<Integer> al,
			BenchmarkPrinter bp, int size, int i) throws IOException {
		testMe(bp, i, al.getClass().getSimpleName() + "["+size+"] remove begining", new BenchmarkCodeTest() {public void testCode() {al.remove(0);}});
	}

	public static void removeMiddle(final List<Integer> al,
			BenchmarkPrinter bp, int size, int i) throws IOException {
		testMe(bp, i, al.getClass().getSimpleName() + "["+size+"] remove middle", new BenchmarkCodeTest() {public void testCode() {al.remove(al.size()/2);}});
	}

	public static void browseIterator(final List<Integer> al,
			BenchmarkPrinter bp, int size, int i) throws IOException {
		testMe(bp, i, al.getClass().getSimpleName() + "["+size+"] browse all using iterator", new BenchmarkCodeTest() {public void testCode() {
			for (Iterator<Integer> iterator = al.iterator(); iterator.hasNext();) {
				if (iterator.next() == 7) {
					break;
				}
			}
		}});
	}

	public static void browseIndex(final List<Integer> al,
			BenchmarkPrinter bp, int size, int i) throws IOException {
		testMe(bp, i, al.getClass().getSimpleName() + "["+size+"] browse all using index", new BenchmarkCodeTest() {public void testCode() {
			for (int j = 0; j < al.size(); j++) {
				if (al.get(j) == 7) {
					break;
				}
			}
		}});
	}

	public static void addMiddle(final List<Integer> al,
			BenchmarkPrinter bp, int size, int i) throws IOException {
		testMe(bp, i, al.getClass().getSimpleName() + "["+size+"] add middle", new BenchmarkCodeTest() {public void testCode() {al.add(al.size()/2, 7);}});
	}

	public static void addEnd(final List<Integer> al,
			BenchmarkPrinter bp, int size, int i) throws IOException {
		testMe(bp, i, al.getClass().getSimpleName() + "["+size+"] add end", new BenchmarkCodeTest() {public void testCode() {al.add(al.size(), 6);}});
	}

	public static void addBegining(final List<Integer> al,
			BenchmarkPrinter bp, int size, int i) throws IOException {
		testMe(bp, i, al.getClass().getSimpleName() + "["+size+"] add begining", new BenchmarkCodeTest() {public void testCode() {al.add(5);}});
	}

	public static List<Integer> fill(List<Integer> list, Integer size) {
		for (int i = 0; i < size; i++) {
			list.add(0);
		}
		return list;
	}

	
	
	
	
	public static void testMe(BenchmarkPrinter printer, int iteration, String testTitle, BenchmarkCodeTest codeToTest) throws IOException {
		BenchmarkResults alResults = new StandardBenchmark(testTitle).testEnteredCode(iteration, codeToTest);
		printer.setBenchmarkResults(alResults);
		System.out.println(printer.getStatsOneLine());
		printer.getStatsOneLineToFile();
	}
	
}
