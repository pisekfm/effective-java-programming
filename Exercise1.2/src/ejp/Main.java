package ejp;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Random;

import benchmark.Benchmark;
import benchmark.BenchmarkCodeTest;
import benchmark.BenchmarkResults;
import benchmark.StandardBenchmark;


public class Main {

    private static final String FILE_NAME = "t";
    private static final int BUFFER_SIZE = 1024 * 1024;	//1MB
    private static final int FILE_SIZE = 200 * 1024 * 1024;	//200MB
	
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		
		final RandomAccessFile randFile = generateRandomFile(FILE_NAME, BUFFER_SIZE);

		final File f = new File(FILE_NAME);

		Benchmark b = new StandardBenchmark("some test");
		BenchmarkResults bufferedReadResults = b.testEnteredCode(1, new BenchmarkCodeTest() {
			@Override
			public void testCode() throws Exception {
				InputStream in1 = new FileInputStream(f);
				byte[] buffer1 = new byte[BUFFER_SIZE];
				while (in1.read(buffer1) != -1) {
					//READING
				}
				in1.close();
			}
		});
		
		BenchmarkResults channelBufferReadResults = b.testEnteredCode(1, new BenchmarkCodeTest() {
			@Override
			public void testCode() throws Exception {
        		FileChannel in2 = new FileInputStream(f).getChannel();
        		ByteBuffer buffer2 = ByteBuffer.allocate(BUFFER_SIZE);
        		while (in2.read(buffer2) != -1) {
        			buffer2.flip();
        			//READING
        			buffer2.clear();
        		}
        		in2.close();
			}
		});

		BenchmarkResults fileMapReadResults = b.testEnteredCode(1, new BenchmarkCodeTest() {
			@Override
			public void testCode() throws Exception {
        		MappedByteBuffer buf = randFile.
        				getChannel().map(FileChannel.MapMode.READ_ONLY, 0, FILE_SIZE);
        		for (int i = 0; i < FILE_SIZE; i++) {
        			buf.get();
        		}
			}
		});
        
		System.out.println(bufferedReadResults);
		System.out.println(channelBufferReadResults);
		System.out.println(fileMapReadResults);
        
	}

	private static RandomAccessFile generateRandomFile(String fileName, int bufferSize) throws FileNotFoundException, IOException {
	    RandomAccessFile randFile = new RandomAccessFile(fileName, "rw");
		Random r = new Random();
		byte[] b = new byte[bufferSize];
		for (int i = 0; i < 200; i++) {
			r.nextBytes(b);
			randFile.write(b, 0, bufferSize);
		}
	    return randFile;
    }

}
