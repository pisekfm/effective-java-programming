package michal.pisarski.ex4.main;

import java.lang.ref.PhantomReference;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.lang.reflect.Constructor;

public class References {

	public static void main(String[] args) throws Exception {

		System.out.println("-------------TESTING SoftReference--------------");
		testReference(SoftReference.class);
		System.out.println("------------------------------------------------");
		System.out.println();

		System.out.println("-------------TESTING WeakReference--------------");
		testReference(WeakReference.class);
		System.out.println("------------------------------------------------");
		System.out.println();

		System.out.println("-----------TESTING PhantomReference-------------");
		testReference(PhantomReference.class);
		System.out.println("------------------------------------------------");
		System.out.println();

	}

	/**
	 * Method for testing a soft, weak and phantom reference...
	 * @param clazz
	 * @throws Exception
	 */
	private static void testReference(Class clazz) throws Exception {

		if (clazz == null) {
			System.err.println("No parameter...");
			return;
		}
		
		if (!clazz.getSuperclass().equals(Reference.class)) {
			System.err.println("Class parameter is not extending Reference class...");
			return;
		}
		
		System.out.println(clazz.getSuperclass());
		
		Object object = new Object() {
			public String toString() {
				return "referenced object";
			}
		};

		System.out.println("...Creating a " + clazz.getSimpleName() + "...");
		Constructor constructor = clazz.getConstructor(Object.class, ReferenceQueue.class);
		Reference reference = (Reference) constructor.newInstance(object, new ReferenceQueue());

		System.out.println("Referenced object equals to: [" + reference.get() + "]");
		System.out.println("Reference is enqueued: [" + reference.isEnqueued() + "]");

		System.out.println("...Running garbage collector...");
		System.gc();

		System.out.println("Referenced object equals to: [" + reference.get() + "]");
		System.out.println("Reference is enqueued: [" + reference.isEnqueued() + "]");

		System.out.println("...Clearing strong reference - object ready for garbage collection...");
		object = null;
		System.out.println("...Running garbage collector...");
		System.gc();

		System.out.println("Referenced object equals to: [" + reference.get() + "]");
		System.out.println("Reference is enqueued: [" + reference.isEnqueued() + "]");

	}

}
